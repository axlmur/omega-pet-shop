// importo la librería de express
const express = require('express');
const app = express();

// importo el archivo de conexión a MongoDB
const miconexion = require('./conexion');

// importo el modelo y las rutas de productos
const rutaProductos = require('./rutas/productos')

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

//Creo el endpoint para interactuar con productos
app.use('/api/productos', rutaProductos)

//implemento el servidor escuchando en el puerto 5000
app.listen(5000, function()
{
    console.log("Prueba - servidor OK! - puerto 5000")
});

// Hago una petición Get por defecto
app.get('/', function(req, res){
    res.send('Servidor corriendo Ok');
    //res.send('<marquee style="color: crimson"> hello world </marquee>')
});

